import { Book } from './book';

export const BookList: Book[] = [
  {  id: 1,
    name: "John.doe@gmail.com",
    year: '2010'         
  },
  {  id: 2,
    name: "jane.doe@gmail.com",
    year: '2012'}
  
];