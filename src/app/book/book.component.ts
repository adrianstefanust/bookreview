import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookList } from '../booksDefault';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  book=  BookList;
  selectedBook = BookList;
  constructor() { }

  ngOnInit(): void {
  }
  onSelect(book: Book): void {
    this.selectedBook = book;
  }

}
