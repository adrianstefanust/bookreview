export interface Book {
    id: number;
    name: string;
    year: string;
  }