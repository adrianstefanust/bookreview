import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  books = [];
  constructor() { }

  addBook(book) {
    this.books.push(book);
  }

  getItems() {
    return this.books;
  }
}
